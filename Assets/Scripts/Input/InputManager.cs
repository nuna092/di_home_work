using System;
using UnityEngine;

namespace ShootEmUp
{
    public sealed class InputManager : MonoBehaviour, IGameUpdateListener , IService
    {
        public event Action<int> OnArrowsPressed;
        public event Action<bool> OnSpacePressed;
               
        void IGameUpdateListener.OnUpdate(float deltaTime)
        {          
            if (Input.GetKeyDown(KeyCode.Space))
                OnSpacePressed?.Invoke(true);

            if (Input.GetKey(KeyCode.LeftArrow))        
                OnArrowsPressed?.Invoke(-1);
                        
            else if (Input.GetKey(KeyCode.RightArrow))           
                OnArrowsPressed?.Invoke(1);     
            else       
               OnArrowsPressed?.Invoke(0);                         
        }
    }
}