using UnityEngine;

public class ServiceLocatorInstaller : MonoBehaviour, IGameFirstInitListener
{
    [SerializeField]
    private MonoBehaviour[] sercvices;
    private GameMachine machine;

    void IGameFirstInitListener.OnInit()
    {
        for (var i = 0; i < sercvices.Length; i++)
        {
            var monoComponents = sercvices[i].GetComponents<MonoBehaviour>();
            for (var j = 0; j < monoComponents.Length; j++)
            {
                ServiceLocator.AddService(monoComponents[j]);
            }
        }

    }

}
