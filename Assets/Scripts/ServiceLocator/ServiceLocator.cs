﻿using System;
using System.Collections.Generic;

public  static class ServiceLocator 
{
    public static readonly List<object> services = new();
    public static List<T> GetServices<T>()
    {
        var resault = new List<T>();
        foreach(var service in services)
        {
            if(service is T tService)
            {
                resault.Add(tService);
            }
        }
        return resault;
    }
    
    public static object GetService (Type serviceType)
    {
        foreach (var service in services)
        {
            if (serviceType.IsInstanceOfType(service))
            {
                return service;
            }

        }
        throw new Exception($"Сервис типа {(serviceType).Name} не найден!");
    }

    public static T GetService<T>()
    {
        foreach(var service in services)
        {
            if(service is T resault)
            {
                return resault;
            }
     
        }
        throw new Exception($"Сервис типа {typeof(T).Name} не найден!");
    }


    public static void AddService(object service)
    {
        services.Add(service);
    }
}
