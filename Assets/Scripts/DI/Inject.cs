using JetBrains.Annotations;
using System;


[MeansImplicitUse]
[AttributeUsage(AttributeTargets.Method)]
public sealed class Inject: Attribute
{
    
}
