using System;
using System.Reflection;


public class DependencyInjector
{
    /// <summary>
    /// ����� ����������� ����� ��������� 
    /// </summary>
    /// <param name="target"></param>
    public static void Inject(object target)
    {
        Type type = target.GetType(); //�������� ��� ������ � �������
        MethodInfo[] methods = type.GetMethods( //�������� ������ ������� � ����� ������
            BindingFlags.Instance | // �������������� ������
            BindingFlags.Public | // ���������
            BindingFlags.NonPublic | // �����������
            BindingFlags.FlattenHierarchy // ������������ ������

            );

        foreach (var method in methods)
        {
            if (method.IsDefined(typeof(Inject))) //���� � ������� ���� ������� [inject]
            {
                InvokeMethod(method, target);
            }
        }

  }

    private static void InvokeMethod(MethodInfo method, object target) //MethodInfo ����� ������� ������ ������ � ������. ���������� ��� ������ � ��� ���������
    {
        ParameterInfo[] parameterInfos = method.GetParameters(); //�������� ��� ��������� ������

        object[] args = new object[parameterInfos.Length];

        for (var i = 0; i < parameterInfos.Length; i++)
        {
            var parameter = parameterInfos[i];
            var type = parameter.ParameterType; //�������� ��� ���������        
            object arg = ServiceLocator.GetService(type); //������� ��� � ������ ������� ������� ���������� ������
            args[i] = arg;
        }

        method.Invoke(target, args); //�������� ����� �� �������, ������� ��� ������ ����������
    }
}
