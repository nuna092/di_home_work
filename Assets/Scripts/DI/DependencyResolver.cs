﻿using UnityEngine;

public class DependencyResolver : MonoBehaviour, IGameSecondInitListener
{
    private void ResolveDependensise(Transform node)
    {
        var behaviours = node.transform.GetComponents<MonoBehaviour>();
        foreach (var behaviour in behaviours)
        {
            DependencyInjector.Inject(behaviour);
        }

        foreach (Transform child in node) //проходимся по дочерним объекстам
        {
            ResolveDependensise(child);
        }
    }

    void IGameSecondInitListener.OnInit()
    {
        ResolveDependensise(transform);      
    }


}
