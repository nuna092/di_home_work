using UnityEngine;

namespace ShootEmUp
{
    public class CharacterMovement : MonoBehaviour, IGameFixedUpdateListener, IGameFinishListener, IGameStartListener
    {
        [SerializeField]
        private Rigidbody2D _rigidbody2D;
      
        private InputManager _keyBoardImput;

        [SerializeField]
        private float speed = 5.0f;

        private int _direction;

        [Inject]
        public void Construct(InputManager inputManager)
        {
            _keyBoardImput = inputManager;
        }

        public void MoveByRigidbodyVelocity(Vector2 vector)
        {
            var nextPosition = _rigidbody2D.position + vector * speed;
             _rigidbody2D.MovePosition(nextPosition);
        }

        void IGameStartListener.OnStartGame()
        {
            _keyBoardImput.OnArrowsPressed += GetHorizontalDirection;
        }
       
        void IGameFixedUpdateListener.OnFixedUpdate(float deltaTime)
        {
            MoveByRigidbodyVelocity(new Vector2(_direction, 0) * deltaTime);
        }

       
        void IGameFinishListener.OnFinishGame()
        {
            _keyBoardImput.OnArrowsPressed -= GetHorizontalDirection;
        }

        private void GetHorizontalDirection(int value)
        {
            _direction = value;
        }

       
    }
}