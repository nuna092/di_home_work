﻿using ShootEmUp;
using UnityEngine;


    public class CharacterHealth : MonoBehaviour, IGameFinishListener, IGameStartListener
{
    private HitPointsComponent _hitpointsComp; 
    private GameMachine _gameManager;


    private void OnCharacterDeath(GameObject _) => _gameManager.FinishGame();


    [Inject]
    private void Construct(GameMachine gameMachine, HitPointsComponent hitPoint ) //заинжектить
    {      
        _gameManager = gameMachine;
        _hitpointsComp = hitPoint;
    }

    void IGameFinishListener.OnFinishGame()
    {
        _hitpointsComp.hpEmpty -= OnCharacterDeath;       
    }

    void IGameStartListener.OnStartGame()
    {
        _hitpointsComp.hpEmpty += OnCharacterDeath;
    }
}
