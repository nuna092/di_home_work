using UnityEngine;

namespace ShootEmUp
{
    public sealed class CharacterShootController : MonoBehaviour, IGameFinishListener, IGameFixedUpdateListener, IGameStartListener
    {
        private BulletFabric _bulletFabric;
        private InputManager _inputKeyBoard; 

        private bool _fireRequired;

        [Inject]
        public void Construct(InputManager keyboard, BulletFabric bulletFabric)
        {
            _inputKeyBoard = keyboard;
            _bulletFabric = bulletFabric;
        }

        void IGameStartListener.OnStartGame()
        {
            _inputKeyBoard.OnSpacePressed += FireReqired;
        }
        void IGameFinishListener.OnFinishGame()
        {           
            _inputKeyBoard.OnSpacePressed -= FireReqired;
        }

        void IGameFixedUpdateListener.OnFixedUpdate(float deltaTime)
        {
            if (_fireRequired)
            {
                _bulletFabric.CreatePlayerBullet();
                _fireRequired = false;
            }
        }

        public void FireReqired(bool value)
        {
            _fireRequired = value;
        }

       
    }
}