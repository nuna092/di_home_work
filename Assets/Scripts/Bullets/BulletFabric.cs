using ShootEmUp;
using UnityEngine;

public class BulletFabric : MonoBehaviour, IService
{
    [SerializeField] private BulletConfig _bulletConfig;
    private BulletSystem _bulletSystem;
    private WeaponComponent _playerWeapon;

    [Inject]
    public void Construct(BulletSystem bulletSystem, WeaponComponent playerWeapon)
    {
        _bulletSystem = bulletSystem;
        _playerWeapon = playerWeapon;
    }

    public void CreatePlayerBullet()
    {
        var bulletArgs = new BulletSystem.Args();
        bulletArgs.isPlayer = true;
        bulletArgs.physicsLayer = (int)_bulletConfig.physicsLayer;
        bulletArgs.color = _bulletConfig.color;
        bulletArgs.damage = _bulletConfig.damage;
        bulletArgs.position = _playerWeapon.Position;
        bulletArgs.velocity = _playerWeapon.Rotation * Vector3.up * _bulletConfig.speed;

        _bulletSystem.FlyBulletByArgs(bulletArgs);
    }

    public void CreateEnemyBullet(Vector2 _position, Vector2 _direction)
    {
        var bulletArgs = new BulletSystem.Args();
        bulletArgs.isPlayer = false;
        bulletArgs.physicsLayer = (int)PhysicsLayer.ENEMY;
        bulletArgs.color = UnityEngine.Color.red;
        bulletArgs.damage = 1;
        bulletArgs.position = _position;
        bulletArgs.velocity = _direction * 2.0f;
        
        _bulletSystem.FlyBulletByArgs(bulletArgs);
    }

}
