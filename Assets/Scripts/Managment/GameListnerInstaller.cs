using UnityEngine;


public class GameListnerInstaller : MonoBehaviour
{
    [SerializeField] private GameObject[] _listnersMono;
    private void Awake() 
        {
            var gameManager = GetComponentInParent<GameMachine>();

        for (var i = 0; i < _listnersMono.Length; i++)
        {
            var monoComponents = _listnersMono[i].GetComponents<MonoBehaviour>();

            for(var j = 0; j < monoComponents.Length; j++)
            {
                if (monoComponents[j] is IGameListener listner)
                    gameManager.AddListener(listner);
            }
        }

    }

}

